FROM node:latest

RUN npm install -g json-server

WORKDIR /data
VOLUME /data

EXPOSE 80
ADD api-json-server.sh /api-json-server.sh
ENTRYPOINT ["bash", "/api-json-server.sh"]
CMD []
