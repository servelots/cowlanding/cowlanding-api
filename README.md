# COWLanding API

## Usage 

There is a [docker image available](https://index.docker.io/u/clue/json-server/), using this image for the first time will start a download automatically.

To Run the container :

```bash
$ docker run -d -p 4001:80 -v /`path to db.json file`/db.json:/data/db.json clue/json-server
```
